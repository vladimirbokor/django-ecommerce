from django.contrib import admin
from .models import Client


class ClientAdmin(admin.ModelAdmin):
    list_display = ['firstname', 'lastname', 'phone', 'email']


admin.site.register(Client, ClientAdmin)

from django.contrib.auth.models import User
from django.db import models

from clients.validators import PhoneRegexValidator


class Client(models.Model):
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        related_name='client',
        verbose_name='Клиент',
        null=True
    )
    firstname = models.CharField(
        max_length=255,
        verbose_name='Имя',
        null=True
    )
    lastname = models.CharField(
        max_length=255,
        verbose_name='Фамилия',
        null=True
    )
    phone = models.CharField(
        max_length=255,
        verbose_name='Телефон',
        validators=[PhoneRegexValidator()],
    )
    email = models.EmailField(
        max_length=255,
        verbose_name='Email для связи',
        null=True
    )
    created_at = models.DateTimeField(
        auto_now_add=True,
        verbose_name='Время создания',
    )

    class Meta:
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'

    def __str__(self):
        return f"{self.firstname} {self.lastname}"



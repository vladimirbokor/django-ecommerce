from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.contrib.auth.models import User
from django import forms
from django.forms import ModelForm

from .models import Client
from .validators import PhoneRegexValidator


class RegistrationForm(UserCreationForm):
    phone = forms.CharField(label="Контактный телефон",
                            validators=[PhoneRegexValidator()])

    class Meta:
        model = User
        fields = ('username', 'phone')

    def save(self, **kwargs):
        user = super().save()

        Client.objects.create(
            user=user,
            phone=self.cleaned_data["phone"],
        )
        return user


class EditProfileForm(ModelForm):
    phone = forms.CharField(label="Контактный телефон")
    email = forms.CharField(label="E-mail для связи", required=False)
    firstname = forms.CharField(label="Имя", required=False)
    lastname = forms.CharField(label="Фамилия", required=False)

    class Meta:
        model = Client
        fields = ('firstname', 'lastname', 'email', 'phone')

# Generated by Django 2.2 on 2019-12-11 13:53

import clients.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('clients', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='client',
            name='phone',
            field=models.CharField(max_length=255, validators=[clients.validators.PhoneRegexValidator()], verbose_name='Телефон'),
        ),
    ]

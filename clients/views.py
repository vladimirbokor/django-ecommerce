from django.views import generic
from django.db.models import Q

from clients.forms import RegistrationForm, EditProfileForm
from clients.models import Client
from django.shortcuts import redirect


class RegisterView(generic.FormView):
    template_name = 'registration/signup.html'
    form_class = RegistrationForm
    success_url = '/accounts/login'

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)


class EditProfileView(generic.UpdateView):
    model = Client
    form_class = EditProfileForm
    template_name = 'clients/edit_profile.html'
    success_url = '/accounts/profile'


class ProfileView(generic.TemplateView):
    template_name = 'clients/profile.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = self.request.user

        try:
            client = Client.objects.get(user=user)
        except Client.DoesNotExist:
            if any((user.is_superuser, user.is_staff)):
                client = Client.objects.create(
                    user=user,
                    firstname="Admin",
                    lastname=user.username,
                    phone="+71111111111",
                )
            else:
                return None

        current_orders = user.order.filter(
            Q(ordered=True) &
            Q(is_bought_out=False)
        )
        past_orders = user.order.filter(
            Q(ordered=True) &
            Q(is_bought_out=True)
        )

        context['client'] = client
        context['current_orders'] = current_orders
        context['past_orders'] = past_orders

        return context

    def render_to_response(self, context, **response_kwargs):
        if context is None:
            return redirect('error')
        return super().render_to_response(context, **response_kwargs)

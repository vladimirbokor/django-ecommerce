from django.contrib import admin

from core.models.auxilary_models import *
from core.models.models import Watch, OrderItem, Order, Price, \
    PickupAddress

# class OrderAdmin(admin.ModelAdmin):
#     list_display = ['user',
#                     'ordered',
#                     'being_delivered',
#                     'received',
#                     'refund_requested',
#                     'refund_granted',
#                     'shipping_address',
#                     'billing_address',
#                     'payment',
#                     'coupon'
#                     ]
#     list_display_links = [
#         'user',
#         'shipping_address',
#         'billing_address',
#         'payment',
#         'coupon'
#     ]
#     list_filter = ['ordered',
#                    'being_delivered',
#                    'received',
#                    'refund_requested',
#                    'refund_granted']
#     search_fields = [
#         'user__username',
#         'ref_code'
#     ]
#     actions = [make_refund_accepted]


class WatchAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}


admin.site.register(Watch, WatchAdmin)
admin.site.register(OrderItem)
# admin.site.register(Order, OrderAdmin)
admin.site.register(Order)


admin.site.register(Color)
admin.site.register(Material)
admin.site.register(GlassMaterial)
admin.site.register(WatchMechanism)
admin.site.register(Watchband)
admin.site.register(WatchCase)
admin.site.register(WatchBrand)
admin.site.register(Price)
admin.site.register(PickupAddress)

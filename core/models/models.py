from django.conf import settings
from django.db import models
from django.shortcuts import reverse

from core.models.auxilary_models import (
    WatchBrand,
    Watchband,
    GlassMaterial,
    WatchCase,
    WatchMechanism
)
from core.utils import create_ref_code


class Watch(models.Model):
    class Meta:
        verbose_name = "Модель часов"
        verbose_name_plural = "Модели часов"

    title = models.CharField(
        verbose_name="Название модели",
        max_length=100
    )
    description = models.TextField(
        verbose_name="Описание от производителя"
    )
    image = models.ImageField()
    slug = models.SlugField()
    brand = models.ForeignKey(
        WatchBrand,
        on_delete=models.CASCADE,
        verbose_name='Название бренда',
        related_name='watch'
    )
    band = models.ForeignKey(
        Watchband,
        on_delete=models.CASCADE,
        verbose_name='Ремешок',
        related_name='watch'
    )
    glass = models.ForeignKey(
        GlassMaterial,
        on_delete=models.CASCADE,
        verbose_name='Стекло',
        related_name='watch'
    )
    case = models.ForeignKey(
        WatchCase,
        on_delete=models.CASCADE,
        verbose_name='Корпус',
        related_name='watch'
    )
    mechanism = models.ForeignKey(
        WatchMechanism,
        on_delete=models.CASCADE,
        verbose_name='Механизм',
        related_name='watch'
    )

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("core:product", kwargs={
            'slug': self.slug
        })

    def get_add_to_cart_url(self):
        return reverse("core:add-to-cart", kwargs={
            'slug': self.slug
        })

    def get_remove_from_cart_url(self):
        return reverse("core:remove-from-cart", kwargs={
            'slug': self.slug
        })


class Price(models.Model):
    class Meta:
        verbose_name = "Цена на модель"
        verbose_name_plural = "Цены на модели"

    watch = models.OneToOneField(
        Watch,
        on_delete=models.CASCADE,
        verbose_name="Модель часов",
        related_name='price'
    )
    price = models.FloatField(
        verbose_name="Цена часов в рублях"
    )

    def __str__(self):
        return f"Цена на {self.watch}: {self.price}"


class OrderItem(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name="order_item",
        verbose_name="Клиент"
    )
    ordered = models.BooleanField(
        default=False,
        verbose_name="Заказано"
    )
    item = models.ForeignKey(
        Watch,
        on_delete=models.CASCADE,
        verbose_name="Товар"
    )
    quantity = models.IntegerField(
        verbose_name="Количество",
        default=1
    )

    def __str__(self):
        return f"{self.quantity} of {self.item.title}"

    def get_final_price(self):
        return self.quantity * self.item.price.price


class PickupAddress(models.Model):
    class Meta:
        verbose_name = "Адрес самовывоза"
        verbose_name_plural = "Адреса самовывоза"

    address = models.TextField(
        verbose_name="Полный адрес"
    )
    phone = models.CharField(
        max_length=20,
        verbose_name="Контактный телефон"
    )

    def __str__(self):
        return self.address


class Order(models.Model):
    CASH_CHOICE = 'cash'
    CARD_CHOICE = 'card'

    PAYMENT_CHOICES = (
        (CASH_CHOICE, 'Наличными'),
        (CARD_CHOICE, 'Картой')
    )

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='order',
        verbose_name="Клиент"
    )
    ref_code = models.CharField(
        verbose_name="Код товара",
        default=create_ref_code,
        max_length=20
    )
    items = models.ManyToManyField(OrderItem)
    created_at = models.DateTimeField(
        verbose_name="Создан",
        auto_now_add=True
    )
    ordered_date = models.DateTimeField(
        verbose_name="Дата заказа",
        null=True
    )
    ordered = models.BooleanField(
        verbose_name="Заказано",
        default=False
    )
    payment = models.CharField(
        verbose_name="Способ оплаты",
        max_length=50,
        choices=PAYMENT_CHOICES,
        default=PAYMENT_CHOICES[0][0]
    )
    pickup_address = models.ForeignKey(
        PickupAddress,
        on_delete=models.SET_NULL,
        null=True,
        verbose_name="С какого адреса заберут заказ"
    )
    is_bought_out = models.BooleanField(
        verbose_name="Выкуплен",
        default=False
    )

    def __str__(self):
        return f"Заказ №{self.ref_code}. Клиент: {self.user.client}"

    def get_total_price(self):
        total = 0
        for order_item in self.items.all():
            total += order_item.get_final_price()
        return total

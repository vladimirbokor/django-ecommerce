from django.db import models


class Color(models.Model):
    class Meta:
        verbose_name = "Каталог цвета"
        verbose_name_plural = "Каталог цветов"

    name = models.CharField(
        verbose_name="Название цвета",
        max_length=50
    )

    def __str__(self):
        return self.name


class Material(models.Model):
    class Meta:
        verbose_name = "Каталог материала"
        verbose_name_plural = "Каталог материалов"

    name = models.CharField(
        verbose_name="Название материала",
        max_length=50
    )

    def __str__(self):
        return self.name


class GlassMaterial(models.Model):
    class Meta:
        verbose_name = "Каталог типа стекла"
        verbose_name_plural = "Каталог типов стекла"

    name = models.CharField(
        verbose_name="Название типа стекла",
        max_length=50
    )

    def __str__(self):
        return self.name


class WatchMechanism(models.Model):
    class Meta:
        verbose_name = "Каталог механизмов"
        verbose_name_plural = "Каталог механизмов"

    name = models.CharField(
        verbose_name="Название механизма",
        max_length=50
    )

    def __str__(self):
        return self.name


class Watchband(models.Model):
    class Meta:
        verbose_name = "Каталог ремешков"
        verbose_name_plural = "Каталог ремешков"

    color = models.ForeignKey(
        Color, on_delete=models.CASCADE,
        related_name='watchband',
        verbose_name="Цвет ремешка"
    )
    material = models.ForeignKey(
        Material, on_delete=models.CASCADE,
        related_name='watchband',
        verbose_name="Материал ремешка"
    )

    def __str__(self):
        return f'Ремешок: {self.color} {self.material}'


class WatchCase(models.Model):
    CIRCLE_CHOICE = 'circle'
    SQUARE_CHOICE = 'square'
    ELLIPSE_CHOICE = 'ellipse'

    SHAPE_CHOICES = (
        (CIRCLE_CHOICE, 'Круглый'),
        (SQUARE_CHOICE, 'Квадратный'),
        (ELLIPSE_CHOICE, 'Овальный'),
    )

    class Meta:
        verbose_name = "Каталог корпусов"
        verbose_name_plural = "Каталог корпусов"

    color = models.ForeignKey(
        Color, on_delete=models.CASCADE,
        related_name='watchcase',
        verbose_name="Цвет корпуса"
    )
    material = models.ForeignKey(
        Material, on_delete=models.CASCADE,
        related_name='watchcase',
        verbose_name="Материал корпуса"
    )
    shape = models.CharField(
        default=SHAPE_CHOICES[0][0],
        choices=SHAPE_CHOICES,
        verbose_name="Форма корпуса",
        max_length=50
    )

    def __str__(self):
        return f'Корпус: {self.get_shape_display()} {self.color} {self.material}'


class WatchBrand(models.Model):
    class Meta:
        verbose_name = "Каталог брендов"
        verbose_name_plural = "Каталог брендов"

    name = models.CharField(
        verbose_name="Название бренда",
        max_length=50
    )
    country = models.CharField(
        verbose_name="Страна производителя",
        max_length=50
    )
    warranty = models.IntegerField(
        verbose_name="Гарантия (в годах)",
    )

    def __str__(self):
        return f'{self.name}'

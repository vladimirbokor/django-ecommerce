from django import forms


from core.models.models import Order, PickupAddress


class CheckoutForm(forms.Form):
    payment_option = forms.ChoiceField(
        widget=forms.RadioSelect, choices=Order.PAYMENT_CHOICES)
    pickup_address = forms.ModelChoiceField(
        queryset=PickupAddress.objects.all()
    )
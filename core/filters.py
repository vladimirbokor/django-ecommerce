import django_filters

from core.models.auxilary_models import WatchBrand, Color, Material, WatchCase
from core.models.models import Watch


class WatchFilter(django_filters.FilterSet):
    price__price = django_filters.RangeFilter(label="Цена в рублях")
    band__color = django_filters.ModelChoiceFilter(
        label="Цвет ремешка",
        queryset=Color.objects.all()
    )
    band__material = django_filters.ModelChoiceFilter(
        label="Материал ремешка",
        queryset=Material.objects.all()
    )
    case__color = django_filters.ModelChoiceFilter(
        label="Цвет корпуса",
        queryset=Color.objects.all()
    )
    case__material = django_filters.ModelChoiceFilter(
        label="Материал корпуса",
        queryset=Material.objects.all()
    )
    case__shape = django_filters.ChoiceFilter(
        label="Форма корпуса",
        choices=WatchCase.SHAPE_CHOICES
    )

    class Meta:
        model = Watch
        fields = ['title', 'mechanism', 'glass', 'brand']

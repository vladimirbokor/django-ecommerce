from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import redirect
from django.shortcuts import render, get_object_or_404
from django.utils import timezone
from django.views.generic import ListView, DetailView, View, TemplateView
from django_filters.views import FilterView

from analytics.models import Feedback
from core.models.models import Watch, OrderItem, Order, PickupAddress
from .filters import WatchFilter
from .forms import CheckoutForm


class CheckoutView(View):
    def get(self, *args, **kwargs):
        try:
            order = Order.objects.get(user=self.request.user, ordered=False)
            form = CheckoutForm()
            context = {
                'form': form,
                'order': order,
            }
            return render(self.request, "checkout.html", context)
        except ObjectDoesNotExist:
            messages.info(self.request, "У вас нет активного заказа")
            return redirect("core:checkout")

    def post(self, *args, **kwargs):
        form = CheckoutForm(self.request.POST or None)
        try:
            order = Order.objects.get(user=self.request.user, ordered=False)
        except ObjectDoesNotExist:
            messages.warning(self.request, "У вас нет активного заказа")
            return redirect("core:order-summary")

        if not form.is_valid():
            messages.error(self.request, "Ошибка. Попробуйте еще раз")
            return redirect("core:order-summary")

        payment_option = form.cleaned_data.get('payment_option')
        pickup_address = form.cleaned_data.get('pickup_address')
        order.payment = payment_option
        order.pickup_address = pickup_address
        order.ordered = True
        order.ordered_date = timezone.now()
        order.save(update_fields=[
            'payment', 'pickup_address', 'ordered', 'ordered_date'
        ])
        order.items.update(ordered=True)
        messages.info(
            self.request,
            f"Заказ был успешно принят. Код вашего заказа {order.ref_code}"
        )
        return redirect("/")


class CatalogView(FilterView):
    model = Watch
    paginate_by = 10
    filterset_class = WatchFilter
    template_name = "catalog.html"


class AboutView(TemplateView):
    template_name = 'about.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        pickup_addresses = PickupAddress.objects.all()
        context['addresses'] = pickup_addresses
        return context


class HomeView(ListView):
    model = Watch
    paginate_by = 10
    template_name = "home.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        feedbacks = Feedback.objects.filter(rating__gt=4).order_by(
            '-created_at')
        context['feedbacks'] = feedbacks
        context['show_model'] = True
        return context


class OrderSummaryView(LoginRequiredMixin, View):
    def get(self, *args, **kwargs):
        try:
            order = Order.objects.get(user=self.request.user, ordered=False)
            context = {
                'object': order
            }
            return render(self.request, 'order_summary.html', context)
        except ObjectDoesNotExist:
            messages.warning(
                self.request,
                "У вас нет активного заказа. Добавьте товар в корзину"
            )
            return redirect("/")


class ItemDetailView(DetailView):
    model = Watch
    template_name = "product.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        user = self.request.user

        items = []
        if not user.is_anonymous:
            orders = Order.objects.filter(user=user, is_bought_out=True)
            items = [item for order in orders for item in order.items.all() if
                     item.item == self.object]
        client_has_ordered_model = True if items else False

        feedbacks = Feedback.objects.filter(watch=self.object) \
            .order_by('-created_at')
        context['feedbacks'] = feedbacks
        context['client_has_ordered_model'] = client_has_ordered_model
        return context


@login_required
def add_to_cart(request, slug):
    item = get_object_or_404(Watch, slug=slug)
    order_item, created = OrderItem.objects.get_or_create(
        item=item,
        user=request.user,
        ordered=False
    )
    order_qs = Order.objects.filter(user=request.user, ordered=False)
    if order_qs.exists():
        order = order_qs[0]
        # check if the order item is in the order
        if order.items.filter(item__slug=item.slug).exists():
            order_item.quantity += 1
            order_item.save()
            messages.info(request, "Количество товара было обновлено")
            return redirect("core:order-summary")
        else:
            order.items.add(order_item)
            messages.info(request, "Этот товар был добавлен в вашу корзину")
            return redirect("core:order-summary")
    else:
        order = Order.objects.create(
            user=request.user, created_at=timezone.now()
        )
        order.items.add(order_item)
        messages.info(request, "Этот товар был добавлен в вашу корзину")
        return redirect("core:order-summary")


@login_required
def remove_from_cart(request, slug):
    item = get_object_or_404(Watch, slug=slug)
    order_qs = Order.objects.filter(
        user=request.user,
        ordered=False
    )
    if order_qs.exists():
        order = order_qs[0]
        # check if the order item is in the order
        if order.items.filter(item__slug=item.slug).exists():
            order_item = OrderItem.objects.filter(
                item=item,
                user=request.user,
                ordered=False
            )[0]
            order_item.quantity = 1
            order_item.save(update_fields=['quantity'])
            order.items.remove(order_item)
            messages.info(request, "Этот товар был удален из вашей корзины")
            return redirect("core:order-summary")
        else:
            messages.info(request, "Этого товара нет в вашей корзине")
            return redirect("core:product", slug=slug)
    else:
        messages.info(request, "У вас нет активного заказа")
        return redirect("core:product", slug=slug)


@login_required
def remove_single_item_from_cart(request, slug):
    item = get_object_or_404(Watch, slug=slug)
    order_qs = Order.objects.filter(
        user=request.user,
        ordered=False
    )
    if order_qs.exists():
        order = order_qs[0]
        # check if the order item is in the order
        if order.items.filter(item__slug=item.slug).exists():
            order_item = OrderItem.objects.filter(
                item=item,
                user=request.user,
                ordered=False
            )[0]
            if order_item.quantity > 1:
                order_item.quantity -= 1
                order_item.save()
            else:
                order.items.remove(order_item)
            messages.info(request, "Количество товара было обновлено")
            return redirect("core:order-summary")
        else:
            messages.info(request, "Этого товара нет в вашей корзине")
            return redirect("core:product", slug=slug)
    else:
        messages.info(request, "У вас нет активного заказа")
        return redirect("core:product", slug=slug)

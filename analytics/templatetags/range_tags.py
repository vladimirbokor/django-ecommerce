from django import template

register = template.Library()


@register.filter(name='feedback_full_stars')
def feedback_full_stars(number):
    return range(number)


@register.filter(name='feedback_empty_stars')
def feedback_empty_stars(number):
    return range(5 - number)

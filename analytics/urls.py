from django.contrib.auth.decorators import login_required
from django.urls import path

from . import views

app_name = 'analytics'

urlpatterns = [
    path('product/<slug>/new_feedback',
         login_required(views.FeedbackNew.as_view()),
         name='feedback_new'),
]
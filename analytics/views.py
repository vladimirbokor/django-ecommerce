from django.contrib import messages
from django.shortcuts import render, redirect
from django.urls import reverse
from django.views import generic

from analytics.models import Feedback
from clients.models import Client
from core.models.models import Order, Watch


class FeedbackNew(generic.CreateView):
    model = Feedback
    template_name = 'analytics/feedback_new.html'
    fields = ('rating', 'text')

    def get_success_url(self):
        return reverse('core:product', kwargs=self.kwargs)

    def get(self, request, *args, **kwargs):
        if not self.is_user_permitted_to_leave_feedback():
            return redirect('core:catalog')
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        slug = self.kwargs.get('slug')
        watch = Watch.objects.get(slug=slug)
        context['watch'] = watch
        return context

    def form_valid(self, form):
        feedback = form.save(commit=False)
        slug = self.kwargs.get('slug')
        watch = Watch.objects.get(slug=slug)

        user = self.request.user
        try:
            client = Client.objects.get(user=user)
        except Client.DoesNotExist:
            return redirect('/')
        feedback.author = client
        feedback.watch = watch
        feedback.save()
        messages.info(self.request, "Отзыв успешно добавлен")
        return super().form_valid(form)

    def is_user_permitted_to_leave_feedback(self):
        user = self.request.user

        if user.is_anonymous:
            return False

        slug = self.kwargs.get('slug')
        orders = Order.objects.filter(user=user, is_bought_out=True)
        items = [item for order in orders for item in order.items.all() if
                 item.item.slug == slug]

        return True if items else False
